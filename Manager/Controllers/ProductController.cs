﻿using Shop.Bussiness.Service;
using Shop.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Controllers
{
    public class ProductController : GenericApiController
    {
        // GET: Product
        public ActionResult Index()
        {
            var model = GetService<SanPhamService>().GetSanPhams();
            return View(model);
        }
        public ActionResult Detail(int id)
        {
            var model = GetService<SanPhamService>().GetSanPhamById(id);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = GetService<SanPhamService>().GetSanPhamById(id);
            return View(model);
        }

        public ActionResult Edit(FormCollection data)
        {
            var model = new SanPham();
            model.Id = Convert.ToInt32(data["Id"]);
            model.NgayNhapHang = DateTime.Parse(data["NgayNhapHang"]);
            model.PhanTramSale = Convert.ToInt32(data["PhanTramSale"]);
            model.Ten = data["Ten"];
            model.TinhTrang = data["TinhTrang"];
            model.Url_Img = data["Url_Img"];
            model.Description = data["Description"];
            model.GiaBan = Convert.ToDecimal(data["GiaBan"]);
            model.GiaNhap = Convert.ToDecimal(data["GiaNhap"]);
            GetService<SanPhamService>().EditSanPham(model);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            var model = new SanPham();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            GetService<SanPhamService>().DeleteSanPham(id);
            return RedirectToAction("Index");
        }

        public ActionResult Create(FormCollection data)
        {
            var model = new SanPham();
            model.NgayNhapHang = DateTime.Parse(data["NgayNhapHang"]);
            model.PhanTramSale = Convert.ToInt32(data["PhanTramSale"]);
            model.Ten = data["Ten"];
            model.TinhTrang = data["TinhTrang"];
            model.Url_Img = data["Url_Img"];
            model.Description = data["Description"];
            model.GiaBan = Convert.ToDecimal(data["GiaBan"]);
            model.GiaNhap = Convert.ToDecimal(data["GiaNhap"]);
            GetService<SanPhamService>().AddSanPham(model);
            return RedirectToAction("Index");
        }
    }
}