﻿using Shop.Bussiness.Service;
using Shop.Common.Models;
using System;
using System.Web.Mvc;

namespace Manager.Controllers
{
    public class TagsController : GenericApiController
    {
        // GET: Tags
        public ActionResult Index()
        {
            var model = GetService<TagService>().GetTags();
            return View(model);
        }

        public ActionResult Edit(FormCollection data)
        {
            var model = new Tag();
            model.Id = Convert.ToInt32(data["Id"]);
            model.Name = data["Name"];
            model.Link = data["Link"];
            GetService<TagService>().AddTag(model);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            GetService<TagService>().DeleteTag(id);
            return RedirectToAction("Index");
        }

        public ActionResult Create(FormCollection data)
        {
            var model = new Tag();
            model.Name = data["Name"];

            GetService<TagService>().AddTag(model);
            return RedirectToAction("Index");
        }
    }
}