﻿using Shop.Bussiness.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Controllers
{
    public class CartController : GenericApiController
    {
        // GET: Cart
        public ActionResult Index()
        {
            var model = GetService<CartService>().GetCarts();
            return View(model);
        }
    }
}