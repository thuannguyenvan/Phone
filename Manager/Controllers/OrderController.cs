﻿using Shop.Bussiness.Service;
using Shop.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Controllers
{
    public class OrderController : GenericApiController
    {
        // GET: Order
        public ActionResult Index()
        {
            var model = GetService<DonHangService>().GetDonHangs();
            return View(model);
        }
        public ActionResult Detail(int id)
        {
            var model = GetService<DonHangService>().GetDonHangById(id);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = GetService<DonHangService>().GetDonHangById(id);
            return View(model);
        }

        public ActionResult Edit(FormCollection data)
        {
            
            var model = new DonHangModel();
            model.Id = Convert.ToInt32(data["Id"]);
            model.Date_Create = DateTime.Now;
            model.DiaChiGiaoHang = data["DiaChiGiaoHang"];
            model.Id_User = Convert.ToInt32(data["PhanTramSale"]);
            model.Complete = Convert.ToBoolean(data["Ten"]);
            model.TinhTrang = data["TinhTrang"];
            model.Id_User = Convert.ToInt32(data["Id_User"]);
            model.Note = data["Note"];
            model.Title = data["Title"];
            model.TongGia = Convert.ToDecimal(data["TongGia"]);
            model.PhiPhu = Convert.ToDecimal(data["PhiPhu"]);
            model.SanPham_Id = data["SanPham_Id"];
            GetService<DonHangService>().EditDonHang(model);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            var model = new SanPham();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            GetService<DonHangService>().DeleteDonHang(id);
            return RedirectToAction("Index");
        }

        public ActionResult Create(FormCollection data)
        {
            var model = new DonHangModel();
            model.Date_Create = DateTime.Now;
            model.DiaChiGiaoHang = data["DiaChiGiaoHang"];
            model.Id_User = Convert.ToInt32(data["PhanTramSale"]);
            model.Complete = Convert.ToBoolean(data["Ten"]);
            model.TinhTrang = data["TinhTrang"];
            model.Id_User = Convert.ToInt32(data["Id_User"]);
            model.Note = data["Note"];
            model.Title = data["Title"];
            model.TongGia = Convert.ToDecimal(data["TongGia"]);
            model.PhiPhu = Convert.ToDecimal(data["PhiPhu"]);
            model.SanPham_Id = data["SanPham_Id"];
            GetService<DonHangService>().AddDonHang(model);
            return RedirectToAction("Index");
        }
    }
}