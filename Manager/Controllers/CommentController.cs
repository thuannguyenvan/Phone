﻿using Shop.Bussiness.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Controllers
{
    public class CommentController : GenericApiController
    {
        // GET: Comment
        public ActionResult Index()
        {

            var model = GetService<CommentService>().GetComments();
            return View(model);
        }


        public ActionResult Delete(int id)
        {
            GetService<CommentService>().DeleteComment(id);
            return RedirectToAction("Index");
        }
    }
}