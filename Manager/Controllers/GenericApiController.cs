﻿using Shop.Common.Ultils;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Manager.Controllers
{
    public class GenericApiController : Controller
    {
        ShopEntities _context;
        public ShopEntities DbContext
        {
            get
            {
                if (_context == null)
                {
                    _context = new ShopEntities();
                }
                return _context;
            }
        }

        public virtual B GetService<B>()
        {
            return (B)typeof(B).GetConstructor(new Type[] { typeof(ShopEntities) }).Invoke(new object[] { this.DbContext });
        }

        public virtual DbRepository<B> GetDbRepository<B>() where B : class
        {
            return new DbRepository<B>(this.DbContext);
        }

      
    }
}