﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Bussiness.Service;
using Shop.Common.Models;

namespace Manager.Controllers
{
    public class UserController : GenericApiController
    {
        // GET: User
        public ActionResult Index()
        {
            var model = GetService<UserService>().GetListUser();
            return View(model);
        }

        public ActionResult Create(FormCollection data)
        {
            var model = new User();
            model.DateCreate =DateTime.Now;
            model.DiaChi = data["DiaChi"];
            model.Email = data["Email"];
            model.PassWord = data["PassWord"];
            model.Phone = data["Phone"];
            model.UserName = data["UserName"];
            model.Role_Id = Convert.ToInt32(data["Role_Id"]);
            GetService<UserService>().AddUser(model);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(FormCollection data)
        {
            var model = new User();
            model.Id = Convert.ToInt32(data["Id"]);
            model.DateCreate = DateTime.Now;
            model.DiaChi = data["DiaChi"];
            model.Email = data["Email"];
            model.PassWord = data["PassWord"];
            model.Phone = data["Phone"];
            model.UserName = data["UserName"];
            model.Role_Id = Convert.ToInt32(data["Role_Id"]);
            GetService<UserService>().EditUser(model);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            GetService<UserService>().Delete(id);
            return RedirectToAction("Index");
        }

    }
}