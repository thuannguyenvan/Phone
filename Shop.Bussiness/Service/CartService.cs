﻿using Shop.Common.Models;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Bussiness.Service
{
    public class CartService:GenericService<CART>
    {
        public CartService(ShopEntities context) : base(context)
        {


        }

        public User GetUser(int? id)
        {
            var user = Find<USER>(id);
            return new User()
            {
                DiaChi = user.DiaChi,
                Email = user.Email,
                Id = user.Id,
                PassWord = user.PassWord,
                Phone = user.Phone,
                UserName = user.UserName,
                DateCreate=user.Date_Cretate
            };
        }

        public List<SanPham> GetSanPhams(string listId)
        {
            var result = new List<SanPham>();
            foreach (var x in listId.Split(',').ToList())
            {
                var sanPham = Find<SANPHAM>(x);
                result.Add(new SanPham()
                {
                    Id = sanPham.Id,
                    Ten = sanPham.Ten,
                    TinhTrang = sanPham.TinhTrang,
                    PhanTramSale = sanPham.PhanTramSale,
                    NgayNhapHang = sanPham.NgayNhapHang,
                    Id_Category = sanPham.Id_Category,
                    GiaNhap = sanPham.GiaNhap,
                    ConHang = sanPham.ConHang,
                    Description = sanPham.Description,
                    GiaBan = sanPham.GiaBan,
                    Url_Img = sanPham.Url_Img
                });
            }

            return result;
        }
        public List<CartModel> GetCarts()
        {
            var result = new List<CartModel>();
            foreach (var item in All.ToList())
            {
                result.Add(new CartModel()
                {
                    User = GetUser(item.Id_User)
                    //ListSanPham = GetSanPhams(item.Id_SanPham)
                });
            }
            return result;
        }

       
    }
}
