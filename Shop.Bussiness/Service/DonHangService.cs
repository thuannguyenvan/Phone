﻿using Newtonsoft.Json.Linq;
using Shop.Common.Models;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Bussiness.Service
{
    public class DonHangService : GenericService<DONHANG>
    {
        public DonHangService(ShopEntities context) : base(context)
        {

        }

        public User GetUser(int? id)
        {
            var user = Find<USER>(id);
            return new User()
            {
                DiaChi = user.DiaChi,
                Email = user.Email,
                Id = user.Id,
                PassWord = user.PassWord,
                Phone = user.Phone,
                UserName = user.UserName,
                DateCreate=user.Date_Cretate
                
            };
        }

        public List<SanPham> GetSanPhams(string listId)
        {
            var result = new List<SanPham>();
            foreach (var x in listId.Split(',').ToList())
            {
                var sanPham = Find<SANPHAM>(x);
                result.Add(new SanPham()
                {
                    Id = sanPham.Id,
                    Ten = sanPham.Ten,
                    TinhTrang = sanPham.TinhTrang,
                    PhanTramSale = sanPham.PhanTramSale,
                    NgayNhapHang = sanPham.NgayNhapHang,
                    Id_Category = sanPham.Id_Category,
                    GiaNhap = sanPham.GiaNhap,
                    ConHang = sanPham.ConHang,
                    Description = sanPham.Description,
                    GiaBan = sanPham.GiaBan,
                    Url_Img = sanPham.Url_Img,
                    Url_Img_Show= sanPham.Url_Img_Show.Split(',').ToList()
            });
            }

            return result;
        }

        public List<DonHang> GetDonHangs()
        {
            var result = new List<DonHang>();
            foreach (var item in All.ToList())
            {
                result.Add(new DonHang()
                {
                    DonHangModel = new DonHangModel()
                    {
                        Complete = item.Complete,
                        Date_Create = item.Date_Create,
                        DiaChiGiaoHang = item.DiaChiGiaoHang,
                        Id = item.Id,
                        Note = item.Note,
                        PhiPhu = item.PhiPhu,
                        TinhTrang = item.TinhTrang,
                        Title = item.Title,
                        TongGia = item.TongGia
                    },
                    User = GetUser(item.Id_User),
                    ListSanPham = GetSanPhams(item.SanPham_Id)
                });
            }
            return result;
        }

        public void DeleteDonHang(int id)
        {
            Delete(id);
            Save();
        }

        public void AddDonHang(DonHangModel donHang)
        {
            InsertAndSave(new DONHANG()
            {
                Complete = donHang.Complete,
                Date_Create = donHang.Date_Create,
                DiaChiGiaoHang = donHang.DiaChiGiaoHang,
                Id = donHang.Id,
                Note = donHang.Note,
                PhiPhu = donHang.PhiPhu,
                TinhTrang = donHang.TinhTrang,
                Title = donHang.Title,
                TongGia = donHang.TongGia,
                Id_User = donHang.Id_User,
                SanPham_Id = donHang.SanPham_Id
                
            });
        }

        public void EditDonHang(DonHangModel donHang)
        {
            var dh = Find<DONHANG>(donHang.Id);
            dh.Complete = donHang.Complete;
            dh.Date_Create = donHang.Date_Create;
            dh.DiaChiGiaoHang = donHang.DiaChiGiaoHang;
            dh.Note = donHang.Note;
            dh.PhiPhu = donHang.PhiPhu;
            dh.TinhTrang = donHang.TinhTrang;
            dh.Title = donHang.Title;
            dh.TongGia = donHang.TongGia;
            dh.Id_User = donHang.Id_User;
            dh.SanPham_Id = donHang.SanPham_Id;
            UpdateAndSave(dh);
        }

        public DonHang GetDonHangById(int id)
        {
            var donhang = Find<DONHANG>(id);
            return new DonHang()
            {
                DonHangModel = new DonHangModel()
                {
                    Complete = donhang.Complete,
                    Date_Create = donhang.Date_Create,
                    DiaChiGiaoHang = donhang.DiaChiGiaoHang,
                    Id = donhang.Id,
                    Note = donhang.Note,
                    PhiPhu = donhang.PhiPhu,
                    TinhTrang = donhang.TinhTrang,
                    Title = donhang.Title,
                    TongGia = donhang.TongGia
                },
                User = GetUser(donhang.Id_User),
                ListSanPham = GetSanPhams(donhang.SanPham_Id)
            };
        }
    }
}
