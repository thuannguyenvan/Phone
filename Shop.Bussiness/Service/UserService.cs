﻿using Shop.Common.Models;
using Shop.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Shop.Bussiness.Service
{
    public class UserService : GenericService<USER>
    {
        public UserService(ShopEntities context) : base(context)
        {
        }

        public List<User> GetListUser()
        {
            var result = new List<User>();
            foreach (var item in All.ToList())
            {
                result.Add(new User() {
                    
                    DiaChi=item.DiaChi,
                    Email=item.Email,
                    PassWord=item.PassWord,
                    Phone=item.Phone,
                    UserName=item.UserName,
                    DateCreate=item.Date_Cretate,
                    Id=item.Id
                });
            }
            return result;
        }


        public void DeleteUser(int id)
        {
            Delete(id);
            Save();
        }

        public void AddUser(User user)
        {
            InsertAndSave(new USER()
            {
                DiaChi = user.DiaChi,
                Email = user.Email,
                PassWord = user.PassWord,
                Phone = user.Phone,
                UserName = user.UserName,
                Date_Cretate = user.DateCreate,
                
            });
        }

        public void EditUser(User user)
        {
            var us = Find(user.Id);
            us.DiaChi = user.DiaChi;
            us.Email = user.Email;
            us.PassWord = user.PassWord;
            us.Phone = user.Phone;
            us.UserName = user.UserName;
            us.Date_Cretate = user.DateCreate;
            UpdateAndSave(us);
        }

        public User GetUserByUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return null;
            var query = from c in All
                        orderby c.Id
                        where c.UserName == username
                        select c;
            var user = query.FirstOrDefault();
            return new User() {
                DiaChi=user.DiaChi,
                Email=user.Email,
                Id=user.Id,
                PassWord=user.PassWord,
                Phone=user.Phone,
                UserName=user.UserName,
                DateCreate = user.Date_Cretate,
                
            };
        }

        public bool Login(string userName , string password)
        {
            var result = All.Where(x => x.UserName == userName).FirstOrDefault();
            return result.PassWord == password ? true : false;
        }


    }
}
