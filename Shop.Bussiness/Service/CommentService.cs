﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Common.Models;
using Shop.Repository;

namespace Shop.Bussiness.Service
{
    public class CommentService: GenericService<COMMENT>
    {
        public CommentService(ShopEntities context) : base(context)
        {

        }

        public User GetUser(int? id)
        {
            var user = Find<USER>(id);
            return new User()
            {
                DiaChi = user.DiaChi,
                Email = user.Email,
                Id = user.Id,
                PassWord = user.PassWord,
                Phone = user.Phone,
                UserName = user.UserName,
                DateCreate=user.Date_Cretate
            };
        }

        public List<CommentModel> GetComments()
        {
            var result = new List<CommentModel>();
            foreach (var item in All.ToList())
            {
                result.Add(new CommentModel()
                {
                   User= GetUser(item.Id_User),
                   Comment = new Comment()
                   {
                       ContentPost=item.ContentPost,
                       Date_Create=item.Date_Create,
                       Position=item.Position,
                       Id_BaiDang=item.Id_BaiDang,
                       Id=item.Id,
                       Id_User=item.Id_User
                   }

                });
            }
            return result;
        }
       
        public void DeleteComment(int id)
        {
            Delete(id);
            Save();
        }

        //public void EditComment(Comment sanPham)
        //{
        //    var sp = Find(sanPham.Id);
        //    sp.Ten = sanPham.Ten;
        //    sp.TinhTrang = sanPham.TinhTrang;
        //    sp.PhanTramSale = sanPham.PhanTramSale;
        //    sp.NgayNhapHang = sanPham.NgayNhapHang;
        //    sp.Id_Category = sanPham.Id_Category;
        //    sp.GiaNhap = sanPham.GiaNhap;
        //    sp.ConHang = sanPham.ConHang;
        //    sp.Description = sanPham.Description;
        //    sp.GiaBan = sanPham.GiaBan;
        //    sp.Url_Img = sanPham.Url_Img;
        //    UpdateAndSave(sp);
        //}
    }
}
