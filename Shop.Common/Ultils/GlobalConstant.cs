﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Ultils
{
    public class GlobalConstant
    {
        public const char LIST_SEPARATOR = ',';
        public const string LIST_SEPARATOR_JOIN = ", ";
        public const int CAPTCHA_LENGTH = 6;
        public const long DB_TRUE = 1;
        public const long DB_FALSE = 0;
        public const int DB_MAXLENGTH = 2000;
        public static double TOKEN_TIMEOUT = 30; //minutes

        public const string HEADER_AUTHORIZATION = "Authorization";
        public const string HEADER_USERNAME = "Username";
        public const string HEADER_PASSWORD = "Password";
        public const string HEADER_USER_AGENT = "User-Agent";

        public const string COMMON_INVALID_USER = "Tên đăng nhập hoặc mật khẩu không đúng";
        public const string COMMON_SSO_INVALID_USER = "Tài khoản chưa được đăng ký trên hệ thống";
        public const string COMMON_SSO_INVALID_AUTHORIZE = "Xác thực không hợp lệ";
        public const string COMMON_INVALID_CAPTCHA = "Mã xác nhận không hợp lệ";
        public const string COMMON_INVALID_ALREADY_EXIST = "Tên đăng nhập đã tồn tại";

        public const long PERMISSION_ID_ADMIN = 1;
        public const long PERMISSION_ID_MASTER = 2;
        public const long PERMISSION_ID_USER = 3;

        public const string EXCEL_PERMISSION_ADMIN = "admin";
        public const string EXCEL_PERMISSION_MASTER = "master";
        public const string EXCEL_PERMISSION_USER = "user";

        public const int EXCEL_CELL_USERNAME = 1;
        public const int EXCEL_CELL_PASSWORD = 2;
        public const int EXCEL_CELL_EMAIL = 3;
        public const int EXCEL_CELL_PHONE = 4;
        public const int EXCEL_CELL_FULLNAME = 5;
        public const int EXCEL_CELL_MACANBO = 6;
        public const int EXCEL_CELL_DONVI = 7;
        public const int EXCEL_CELL_PERMISSION = 8;

        public const string TYPE_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public static class DB_FILE_ARCHIVE
        {
            public const long STATUS_TEMP = 1;
        }
    }
}
