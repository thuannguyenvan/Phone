﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Models
{
    public class DonHang
    {
        public List<SanPham> ListSanPham { get; set; }
        public DonHangModel DonHangModel { get; set; }
        public User User { get; set; }
    }
    public class DonHangModel : Base
    {
        public string Title { get; set; }
        public string SanPham_Id { get; set; }
        public int Id_User { get; set; }
        public DateTime? Date_Create { get; set; }
        public decimal? TongGia { get; set; }
        public string DiaChiGiaoHang { get; set; }
        public string TinhTrang { get; set; }
        public string Note { get; set; }
        public decimal? PhiPhu { get; set; }
        public bool? Complete { get; set; }
    }
}
