﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Models
{

   public class CartModel
    {
        public List<SanPham> ListSanPhams { get; set; }
        public User User { get; set; }
    }
   public class Cart : Base
    {
        public int Id_User { get; set; }
        public int Id_SanPham { get; set; }
    }
}
