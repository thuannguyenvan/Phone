﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Models
{
    public class Comment :Base
    {
        public Nullable<int> Id_BaiDang { get; set; }
        public Nullable<int> Id_User { get; set; }
        public Nullable<System.DateTime> Date_Create { get; set; }
        public string ContentPost { get; set; }
        public string Position { get; set; }
    }

    public class CommentModel
    {
        public Comment Comment { get; set; }
        public User User { get; set; }
    }
}
